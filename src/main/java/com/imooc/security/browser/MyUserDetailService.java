/**
 * 
 */
package com.imooc.security.browser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

/**
 * @author 0
 *
 */
@Component
public class MyUserDetailService implements UserDetailsService {

	private Logger logger=LoggerFactory.getLogger(getClass());
	
	@Autowired
	private PasswordEncoder PasswordEncoder;
	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
	 */
	@Override
	public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
		logger.info("登录用户名是======="+userName);
		//根据查找到的用户信息判断用户是否被冻结
		//下面这个密码是从数据库里拿出来的
		String password=PasswordEncoder.encode("123");
		logger.info("数据里密码是"+password);
		return new User(userName,password,
				true,true,true,true
				, AuthorityUtils.commaSeparatedStringToAuthorityList("admin"));
	}

}
